package co.simplon.alt3.vehicles.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import co.simplon.alt3.vehicles.VehicleType;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Vehicle {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer id;
    
    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private String color;

    // @Column(nullable = false)
    // private Date dateOfPurchase;

    @Column(nullable = false)
    private Double priceOfPurchase;

    @Column(nullable = false)
    private Double resalePrice;

    @Column(nullable = false)
    private Integer wheels;

    @Column(columnDefinition = "ENUM('MOTO', 'BICYCLE', 'CAR')")
    @Enumerated(EnumType.STRING)
    private VehicleType vehicleType;
    
    
    public Vehicle(VehicleType type){
        this.type = type;
        getVehicle();
    }


    private void getVehicle(){

    }

    private VehicleType type= null;

    public Vehicle() {
    }


    public VehicleType getType(){
        return type;
    }

    public void setType(VehicleType type){
        this.type = type;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    // public Date getDateOfPurchase() {
    //     return dateOfPurchase;
    // }

    // public void setDateOfPurchase(Date dateOfPurchase) {
    //     this.dateOfPurchase = dateOfPurchase;
    // }


    public Integer getWheels() {
        return wheels;
    }

    public void setWheels(Integer wheels) {
        this.wheels = wheels;
    }


    public Double getPriceOfPurchase() {
        return priceOfPurchase;
    }


    public void setPriceOfPurchase(Double priceOfPurchase) {
        this.priceOfPurchase = priceOfPurchase;
    }


    public Double getResalePrice() {
        return resalePrice;
    }


    public void setResalePrice(Double resalePrice) {
        this.resalePrice = resalePrice;
    }


    public VehicleType getVehicleType() {
        return vehicleType;
    }


    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }


}
