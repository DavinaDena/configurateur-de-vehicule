package co.simplon.alt3.vehicles.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import co.simplon.alt3.vehicles.VehicleType;

@Entity
public class Bicycle extends Vehicle{
    
    @Column(nullable = false)
    private String saddle;

    @Column(nullable = false)
    private String typeOfFrame;

    @Column(nullable = false)
    private String typeOfBicycle;

    public Bicycle(){
        super(VehicleType.Bicycle);
        construct();
    }

    protected void construct(){
        System.out.println("this is a bycicle");
    }

    public String getSaddle() {
        return saddle;
    }

    public void setSaddle(String saddle) {
        this.saddle = saddle;
    }

    public String getTypeOfFrame() {
        return typeOfFrame;
    }

    public void setTypeOfFrame(String typeOfFrame) {
        this.typeOfFrame = typeOfFrame;
    }

    public String getTypeOfBicycle() {
        return typeOfBicycle;
    }

    public void setTypeOfBicycle(String typeOfBicycle) {
        this.typeOfBicycle = typeOfBicycle;
    }
}
