package co.simplon.alt3.vehicles.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import co.simplon.alt3.vehicles.VehicleType;

@Entity

public class Moto extends Vehicle {

    @Column(nullable = false)
    private String power;

    @Column(nullable = false)
    private String typeOfMoto;

    public Moto() {
        super(VehicleType.Moto);
        construct();
    }

    protected void construct() {
        System.out.println("this is a moto");
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getTypeOfMoto() {
        return typeOfMoto;
    }

    public void setTypeOfMoto(String typeOfMoto) {
        this.typeOfMoto = typeOfMoto;
    }

}
