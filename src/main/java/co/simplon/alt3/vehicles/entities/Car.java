package co.simplon.alt3.vehicles.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

import co.simplon.alt3.vehicles.VehicleType;

@Entity
public class Car extends Vehicle{
    

    @Column(nullable = false)
    private Integer doors;

    @Column(nullable = false)
    private Integer seats;

    @Column(nullable = false)
    private String typeOfCar;

    @Column(nullable = false)
    private Integer windows;

    @Column(nullable = false)
    private Integer power;

    @Column(nullable = false)
    private Boolean transmission;

    public Car(){
        super(VehicleType.Car);
        construct();
    }

    protected void construct(){
        System.out.println("this is a car");
    }




    public Integer getDoors() {
        return doors;
    }

    public void setDoors(Integer doors) {
        this.doors = doors;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public String getTypeOfCar() {
        return typeOfCar;
    }

    public void setTypeOfCar(String typeOfCar) {
        this.typeOfCar = typeOfCar;
    }

    public Integer getWindows() {
        return windows;
    }

    public void setWindows(Integer windows) {
        this.windows = windows;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Boolean getTransmission() {
        return transmission;
    }

    public void setTransmission(Boolean transmission) {
        this.transmission = transmission;
    }

}
