package co.simplon.alt3.vehicles.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import co.simplon.alt3.vehicles.entities.Bicycle;

@RepositoryRestResource
public interface BicycleRepository  extends JpaRepository <Bicycle, Integer>{
    
}
