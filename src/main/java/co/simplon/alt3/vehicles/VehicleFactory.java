package co.simplon.alt3.vehicles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.simplon.alt3.vehicles.entities.Bicycle;
import co.simplon.alt3.vehicles.entities.Car;
import co.simplon.alt3.vehicles.entities.Moto;
import co.simplon.alt3.vehicles.entities.Vehicle;
import co.simplon.alt3.vehicles.repository.BicycleRepository;
import co.simplon.alt3.vehicles.repository.CarRepository;
import co.simplon.alt3.vehicles.repository.MotoRepository;

@Component
public class VehicleFactory {

    @Autowired
    MotoRepository motoRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    BicycleRepository bicycleRepository;

    public Vehicle getVehicle(int id, String type){
        switch (type){
            case "moto":
            Moto moto= motoRepository.findById(id).get();
            return moto;

            case "car":
            Car car= carRepository.findById(id).get();
            return car;

            case "bicycle":
            Bicycle bicycle= bicycleRepository.findById(id).get();
            return bicycle;

            default:
            return null;
        }
    }



    public Vehicle createVehicles(Vehicle vehicle){
        switch(vehicle.getType().toString().toLowerCase()){
            case "moto":
            Moto moto= motoRepository.save((Moto)vehicle);
            return moto;


            case "car":
            Car car= carRepository.save((Car)vehicle);
            return car;

            case "bicycle":
            Bicycle bicycle= bicycleRepository.save((Bicycle)vehicle);
            return bicycle;

            default:
            return null;
        }
    }



    public static char[] getVehicle(VehicleType car) {
        return null;
    }

    
}
