package co.simplon.alt3.vehicles;

public enum VehicleType {
    
    Moto, Car, Bicycle
}
