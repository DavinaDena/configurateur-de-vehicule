package co.simplon.alt3.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt3.vehicles.VehicleFactory;
import co.simplon.alt3.vehicles.entities.Moto;
import co.simplon.alt3.vehicles.repository.MotoRepository;

@RestController
@RequestMapping("/api/vehicle")
public class MotoController {

    @Autowired
    private MotoRepository motoRepository;
    
    @Autowired
    VehicleFactory factory;

    @PostMapping("/moto")
    public Moto postMoto(@RequestBody Moto newMoto){
        return motoRepository.save(newMoto);
    }
    
}
