package co.simplon.alt3.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt3.vehicles.entities.Car;
import co.simplon.alt3.vehicles.repository.CarRepository;

@RestController
@RequestMapping("/api/vehicle")
public class CarController {
    
    @Autowired
    private CarRepository carRepository;

    @PostMapping("/car")
    public Car postCar(@RequestBody Car newCar){
        return carRepository.save(newCar);
    }
}
