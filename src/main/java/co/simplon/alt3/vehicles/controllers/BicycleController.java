package co.simplon.alt3.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt3.vehicles.entities.Bicycle;
import co.simplon.alt3.vehicles.repository.BicycleRepository;

@RestController
@RequestMapping("/api/vehicle")
public class BicycleController {
    
    @Autowired
    private BicycleRepository bicycleRepository;

    @PostMapping("/bicycle")
    public Bicycle postBicycle(@RequestBody Bicycle newBicycle){
        return bicycleRepository.save(newBicycle);
    }
}
