package co.simplon.alt3.vehicles.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.alt3.vehicles.VehicleFactory;
import co.simplon.alt3.vehicles.entities.Vehicle;
import co.simplon.alt3.vehicles.repository.VehicleRepository;

@RestController
@RequestMapping("/api/vehicle")
public class VehicleController {
    
    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    VehicleFactory factory;

    //Get All Vehicles 
    @GetMapping
    public List<Vehicle> all(){
        return vehicleRepository.findAll();
    }

    //Get Vehicle by ID
    @GetMapping("/{id}")
    public Vehicle oneVehicle(@PathVariable int id, @RequestParam String type){
        Vehicle vehicle = factory.getVehicle(id, type);
        if(vehicle == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return vehicle;
    }

    //Post a Vehicle
    // @PostMapping("/")
    // public Vehicle postVehicle(@RequestBody Vehicle newVehicle){
    //     return vehicleRepository.save(newVehicle);
    // }

    //Delete a Vehicle
    // @DeleteMapping("/{id}")
    // void deleteVehicle (@PathVariable int id){
    //     vehicleRepository.deleteById(id);
    // }

    //Update a Vehicle
    //@PutMapping




}
