package co.simplon.alt3.vehicles;

import org.junit.jupiter.api.Test;

public class VehiclesFactoryTests {
    @Test
    void testSendVehicles() {
        System.out.println(VehicleFactory.getVehicle(VehicleType.Moto));
        System.out.println(VehicleFactory.getVehicle(VehicleType.Car));
        System.out.println(VehicleFactory.getVehicle(VehicleType.Bicycle));
    }
}
