Configurateur de véhicule

For this project we had to create an application for a resale of vehicles.
The intention of this exercise is to put in practice different design patterns.

Design patterns

There is three types of design patterns:
- Creational Patterns
- Structural Patterns
- Behavioral Patterns

For my application I used:

Creational Pattern - Factory Method
 - For this method we define an interface and allow subclasses decide which object to instantiate - meaning is the subclasses that will create the object.

Example in exercise: For my application I have a interface/superclass named Vehicles. This one is connected to Car, Moto and Bicycle by inheritance. This last three classes are subclasses of Vehicles and share some of the same parameters. 
So if I want to add a new vehicle, I'll chose which subclass will be in charge of creating it instead of touching the superclass. This will make my application easier to manipulate since in the future I might want to add more type of vehicles without needing to touch the interface/superclass.

Creational Patter - Builder Method
 - With this method we can produce different types and representations of an object using the same construction mode - meaning we can have the same object structure but still finish with a different object

 Example in exercise: I decided to use a builder for the contruction of a vehicle type car. All cars will have some parameters common to each others but, some parameters are optional or just plane different. That's why I put the builder in practice so I can create the same object (car), but totally different from one another without being restricted to the parameters that I applied in my application.

 